package com.spring.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String [] args){

//        //不使用Spring框架测试
//
//        //创建User对象
//        User user = new User();
//
//        //调用user的setName（）方法
//        user.setName("Spring");
//
//        //调用user的sayHello（）方法
//        user.sayHello();


        //使用Spring框架测试
        //1、加载spring配置文件，根据创建对象
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        //2、得到配置创建的对象
        User user = (User) applicationContext.getBean("user");
        //3、调用sayHello()方法
        user.sayHello();

    }
}
