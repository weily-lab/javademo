package spring.jdbc;

import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import spring.entity.User;
import spring.tools.MyRowMapper;

import java.sql.*;
import java.util.List;

public class JdbcTemplateQueryDemo {

    //jdbc实现代码
    @Test
    public void testJdbc(){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        //加载驱动
        try{
            Class.forName("com.mysql.jdbc.Driver");
            //创建连接
            connection = DriverManager.getConnection("jdbc:mysql:///test","root","root");
            //编写SQL语句
            String sql = "select * from student where name=?";
            //预编译SQL
            preparedStatement = connection.prepareStatement(sql);
            //设置参数值
            preparedStatement.setString(1,"Bob");
            resultSet = preparedStatement.executeQuery();
            //遍历结果集
            while (resultSet.next()){
                //得到返回结果值
                String username = resultSet.getString("name");
                String password = resultSet.getString("sex");
                String address = resultSet.getString("address");
                String sex = resultSet.getString("sex");
                int id = resultSet.getInt("id");
                System.out.println(username);
                System.out.println(password);
                System.out.println(sex);
                System.out.println(address);
                System.out.println(id);

            }
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            try {
                resultSet.close();
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    //查询表中有多少条记录
    @Test
    public void testCount() {
        //设置数据库信息
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql:///test");
        dataSource.setUsername("root");
        dataSource.setPassword("root");

        //创建jdbcTemplate对象，设置数据源
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        //调用方法得到记录数
        String sql = "select count(*) from student";

        //调用jdbcTemplate方法得到记录
/*       queryForObject(String sql,Class<T> requiredType)
 *      第一个参数：sql语句
 *      第二个参数：返回类型的class
 */
        int count = jdbcTemplate.queryForObject(sql, Integer.class);
        System.out.println(count);
    }

    //查询返回对象
    @Test
    public void testObject(){
        //设置数据库信息
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql:///test");
        dataSource.setUsername("root");
        dataSource.setPassword("root");

        //创建jdbcTemplate对象，设置数据源
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        //写SQL语句，根据username查询
        String sql = "select * from student where name=?";

        //调用jdbcTemplate的方法实现
        /*
        queryForObject(String sql, RowMapper<T> rowMapper,Object ... args)
        第一个参数：SQL语句
        第二个参数：RowMapper，一个接口，类似于dbutil中的接口
        第三个参数：可变参数

        注：第二个参数是接口RowMapper，需要自己写类实现接口，自己做数据封装
         */
        User user = jdbcTemplate.queryForObject(sql,new MyRowMapper(),"Bob");
        System.out.println(user);
    }

    //查询所有对象list集合
    @Test
    public void testList(){

        //设置数据库信息
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql:///test");
        dataSource.setUsername("root");
        dataSource.setPassword("root");

        //创建jdbcTemplate对象，设置数据源
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        //写SQL语句，根据username查询
        String sql = "select * from student";
        /*
        query(String sql,RowMapper<T> rowMapper,Object ... args)
        第一个参数：SQL语句
        第二个参数：RowMapper接口，自己写类实现数据封装
        第三个参数：可变参数
         */

        //调用jdbcTemplate的query()方法
        List<User> list = jdbcTemplate.query(sql,new MyRowMapper());
        System.out.println(list);
    }

}

