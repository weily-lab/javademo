package spring.jdbc;

import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class JdbcTemplateDemo {

    //添加操作
    @Test
    public void add(){
        //设置数据库信息
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql:///test");
        dataSource.setUsername("root");
        dataSource.setPassword("root");

        //创建jdbcTemplate对象，设置数据源
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        //调用jdbcTemplate对象的方法实现添加操作

        //创建sql语句
        String sql = "insert into student values(?,?,?,?,?)";
        int rows = jdbcTemplate.update(sql,"Tom","man","Beijing","123456",1);
        System.out.println(rows);

    }

    //修改操作
    @Test
    public void update(){
        //设置数据库信息
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql:///test");
        dataSource.setUsername("root");
        dataSource.setPassword("root");

        //创建jdbcTemplate对象，设置数据源
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        //调用jdbcTemplate对象的方法实现修改操作
        String sql = "update student set name=? where address=?";
        int rows = jdbcTemplate.update(sql,"Lucy","NewYork");
        System.out.println(rows);
    }

    //删除操作
    @Test
    public void delete(){
        //设置数据库信息
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql:///test");
        dataSource.setUsername("root");
        dataSource.setPassword("root");

        //创建jdbcTemplate对象，设置数据源
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        //调用jdbcTemplate对象的方法实现删除操作
        String sql = "DELETE FROM student WHERE name=?";
        int rows = jdbcTemplate.update(sql,"Lucy");
        System.out.println(rows);
    }

}
