package com.example;

public class HelloWorldSpring {
    private String name;

    public HelloWorldSpring(){
        System.out.println("This is HelloWorldSpring constructor.");
    }

    public void setName(String name) {
        System.out.println("This is HelloWorldSpring setName().");
        this.name = name;
    }

    public void sayHello(){
        System.out.println("This is HelloWorldSpring sayHello().");
        System.out.println("hello "+name);
    }
}
