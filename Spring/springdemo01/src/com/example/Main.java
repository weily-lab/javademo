package com.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String [] args){
        //不使用框架调用HelloWorldSpring 中的sayHello方法
//        HelloWorldSpring helloWorldSpring = new HelloWorldSpring();
//        helloWorldSpring.setName("Bob");
//        helloWorldSpring.sayHello();

        //使用Spring框架
        //1、创建一个Spring的IOC容器对象
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-config.xml");
        System.out.println("This is Main ApplicationContext.");

        //2、从IOC容器中获取Bean实例
        HelloWorldSpring helloWorldSpring = (HelloWorldSpring) applicationContext.getBean("helloWorldSpring");
        System.out.println("This is HelloWorldSpring helloWorldSpring.");

        //3、调用sayHello()方法
        helloWorldSpring.sayHello();
        System.out.println("This is helloWorldSpring sayHello()........");

    }
}
