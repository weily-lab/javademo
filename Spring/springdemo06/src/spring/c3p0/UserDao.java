package spring.c3p0;

import org.springframework.jdbc.core.JdbcTemplate;

public class UserDao {

    //得到jdbcTemplate对象
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    //添加操作
    public void add() {
        //创建jdbcTemplate对象
//        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        String sql = "insert into student(NAME,password,sex,id,address) values(?,?,?,?,?)";
        jdbcTemplate.update(sql,"Lucy","123456","girl",3,"Shanghai");
    }
}
