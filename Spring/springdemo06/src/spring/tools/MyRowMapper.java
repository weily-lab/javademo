//package spring.tools;
//
//import org.springframework.jdbc.core.RowMapper;
//import spring.entity.User;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//public class MyRowMapper implements RowMapper<User> {
//
//    @Override
//    public User mapRow(ResultSet resultSet, int num) throws SQLException {
//        //1、从结果集中把数据拿到
//        String username = resultSet.getString("name");
//        String password = resultSet.getString("password");
//        String sex = resultSet.getString("sex");
//        String address = resultSet.getString("address");
//        int id = resultSet.getInt("id");
//        //2、把得到的数据封装到对象中
//        User user = new User();
//        user.setUsername(username);
//        user.setSex(sex);
//        user.setPassword(password);
//        user.setId(id);
//        user.setAddress(address);
//        return user;
//    }
//}
