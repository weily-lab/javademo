package spring.dao;

import org.springframework.jdbc.core.JdbcTemplate;

public class OrdersDao {

//    注入jdbcTemplate
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    /*
    做对数据库的操作，不写业务操作
     */

    //减少金额
    public void lessMoney(){
        String sql = "update account set salary=salary-? where name=?";
        jdbcTemplate.update(sql,100,"Tom");
    }

    //增加金额
    public void moreMoney(){
        String sql = "update account set salary=salary+? where name=?";
        jdbcTemplate.update(sql,100,"John");
    }
}
