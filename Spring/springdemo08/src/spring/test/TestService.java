package spring.test;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import spring.service.OrdersService;

public class TestService {

    @Test
    public void testService(){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        OrdersService ordersService = (OrdersService) applicationContext.getBean("ordersService");
        ordersService.accountMonry();
    }

}
