package spring.service;

import org.springframework.transaction.annotation.Transactional;
import spring.dao.OrdersDao;

@Transactional
public class OrdersService {
    private OrdersDao ordersDao;

    public void setOrdersDao(OrdersDao ordersDao) {
        this.ordersDao = ordersDao;
    }

    //调用dao方法
    /*
    业务逻辑层
     */

    //转账业务
    public void accountMonry(){

        ordersDao.lessMoney();  //减少金额

        int i = 10/0;   //模拟出现异常

        ordersDao.moreMoney();  //增加金额
    }
}
