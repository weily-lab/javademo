package demo.spring.service;

import demo.spring.dao.UserDao;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component(value = "userService")
public class UserService {

    //得到dao对象
    //1、定义dao类型属性
    //在dao属性上面使用注解，完成对象注入
//    @Autowired
//    private UserDao userDao;
    //使用注解方式不需要set方法

//    name 属性值写注解创建dao对象value值
    @Resource(name = "userDao")
    private UserDao userDao;

    public void add(){
        System.out.println("service add()........");
    }
}
