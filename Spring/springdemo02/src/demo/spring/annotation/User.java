package demo.spring.annotation;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(value = "user")      //相当于 <bean id="user" class="User全路径"/>
@Scope(value = "prototype")     //默认是单实例
public class User {

    public void add(){
        System.out.println("add()..........");
    }

}
