package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Student;
import util.DBUtil;

public class StudentDAO {
	
	public Student queryStudent(String name){
		Student student = null;
		Connection connection = DBUtil.getConn();
		String sql = "select name,sex,address,password from student where name=?";
		try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                student = new Student();
                student.setName(resultSet.getString("name"));
                student.setSex(resultSet.getString("sex"));
                student.setAddress(resultSet.getString("address"));
                student.setPassword(resultSet.getString("password"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
		return student;
	}
	
	public static void main(String[] args) {
		StudentDAO stuDAO = new StudentDAO();
		Student student = null;
		student = stuDAO.queryStudent("tom");
		System.out.println(student.getAddress());
	}
}
