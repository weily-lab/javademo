package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.StudentDAO;
import model.Student;

public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = "";
		String password = "";
		Student student = null;
		
		
		username = request.getParameter("username");
		password = request.getParameter("password");
		PrintWriter out = response.getWriter();
		
		StudentDAO studentDAO = new StudentDAO();
		student = studentDAO.queryStudent(username);
		if(student.getPassword().equals(password)){
			out.println("登录成功");
		}
		else {
			out.print("登录失败");
		}
	}
	
}
