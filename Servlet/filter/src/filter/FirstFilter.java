package filter;


import javax.servlet.*;
import java.io.IOException;

public class FirstFilter implements Filter{


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("init first.....");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("start first......");
        filterChain.doFilter(servletRequest,servletResponse);
        System.out.println("end first......");
    }

    @Override
    public void destroy() {
        System.out.println("destroy first......");
    }
}
