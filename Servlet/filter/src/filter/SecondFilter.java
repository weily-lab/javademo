package filter;

import javax.servlet.*;
import java.io.IOException;

public class SecondFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("init second filter......");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        System.out.println("start second ......");

        filterChain.doFilter(servletRequest,servletResponse);
        System.out.println("end second .......");
    }

    @Override
    public void destroy() {

        System.out.println("destroy second ....");
    }
}
